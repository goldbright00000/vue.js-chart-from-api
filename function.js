//pie-chart
Vue.component('pie-chart', {
	extends: VueChartJs.Pie,
	props: ['keylist', 'valuelist'],
	watch: {
		valuelist: function () {
			this.renderChart({
				labels: this.keylist,
				datasets: [
					{
						label: 'Data One',
						backgroundColor: ["#41B883", "#E46651", "#00D8FF"],
						data: this.valuelist
					}
				]
			}, {responsive: true, maintainAspectRatio: false})
		}
	}
})

//bar-chart
Vue.component('bar-chart', {

	extends: VueChartJs.Bar,
	props: ['keylist', 'valuelist'],
	watch: {
		valuelist: function () {
			this.renderChart({
				labels: this.keylist,
				datasets: [
					{
						label: 'Data One',
						backgroundColor: ["#41B883", "#E46651", "#00D8FF"],
						data: this.valuelist
					}
				]
			}, {responsive: true, maintainAspectRatio: false})
		}
	}
  
}) 

//stack1-chart
Vue.component('stack1-chart', {
	extends: VueChartJs.Bar,
	props: ['keylist', 'valuelist'],
	watch: {
		valuelist: function () {
			this.renderChart({
				labels: this.keylist,
				datasets: [
					{
						label: 'Data One',
						backgroundColor: ["#41B883", "#E46651", "#00D8FF"],
						data: this.valuelist
					}
				]
			}, {responsive: true, maintainAspectRatio: false})
		}
	}
  
})

//stack2-chart
Vue.component('stack2-chart', {
	extends: VueChartJs.Bar,
	mounted () {
		var vm = this;
		var keylist =[];
		var valuelist =[];
		axios
			.get('https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?g=4')
			.then(response => {
				vm.userdata = response.data[0]
				for (var key in response.data[0]) {
					keylist.push(key);
					valuelist.push(response.data[0][key]);
				}
				vm.renderChart({
					labels: keylist,
					datasets: [
						{
							label: 'Data One',
							backgroundColor: ["#41B883", "#E46651", "#00D8FF"],
							data: valuelist
						}
					]
				}, {responsive: true, maintainAspectRatio: false})
			})	
	}
  
})

var myapp = new Vue({
	el: '#myApp',
	data: {
		optionList1: [],
		optionList2: [],
		optionList3: [],
		pieChartData: Object,
		barChartData: Object,
		stack1ChartData: Object,
		select1: 0,
		select2: 0,
		select3: 0,
	},
	
	mounted() {
		this.renderSelect1()
		this.renderSelect2()
		this.renderSelect3()
		this.initalizePieChart()
		this.initalizeBarChart()
		this.initalizeStack1Chart()
		
	},
	methods: {
		renderSelect1() {
			axios
			.get('https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?d=dr')
			.then(response => (this.optionList1 = response.data))
		},
		renderSelect2() {
			axios
			.get('https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?d=se')
			.then(response => (this.optionList2 = response.data))
		},
		renderSelect3() {
			axios
			.get('https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?d=ag')
			.then(response => (this.optionList3 = response.data))
		},

		refreshPieChart() {
			var piechart_select = '';
			if (this.select1) {
				piechart_select = this.select1;
			}
			axios
			.get(`https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?g=1&dr=${piechart_select}`)
			.then(response => {
				var keylist =[];
				var valuelist =[];
				for (var key in response.data[0]) {
					keylist.push(key);
					valuelist.push(response.data[0][key]);
				}
				this.pieChartData = {
					keylist: keylist,
					valuelist: valuelist
				}
			})
		},
		refreshBarChart(){
			var barchart_select = '';
			if (this.select2) {
				barchart_select = this.select2;
			}
			axios
			.get(`https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?g=2&dr=${barchart_select}`)
			.then(response => {
				var keylist =[];
				var valuelist =[];
				for (var key in response.data[0]) {
					keylist.push(key);
					valuelist.push(response.data[0][key]);
				}
				this.barChartData = {
					keylist: keylist,
					valuelist: valuelist
				}
			})

		},
		refreshStack1Chart(){
			var stack1chart_select = '';
			if (this.select3) {
				stack1chart_select = this.select3;
			}
			axios
			.get(`https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?g=3&dr=${stack1chart_select}`)
			.then(response => {
				console.log(response)
				var keylist =[];
				var valuelist =[];
				for (var key in response.data[0]) {
					keylist.push(key);
					valuelist.push(response.data[0][key]);
				}
				this.stack1ChartData = {
					keylist: keylist,
					valuelist: valuelist
				}
			})

		},
		initalizePieChart() {
			axios
			.get(`https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?g=1`)
			.then(response => {
				var keylist =[];
				var valuelist =[];
				for (var key in response.data[0]) {
					keylist.push(key);
					valuelist.push(response.data[0][key]);
				}
				this.pieChartData = {
					keylist: keylist,
					valuelist: valuelist
				}
			})
		},
		initalizeBarChart(){
			
			axios
			.get(`https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?g=2`)
			.then(response => {
				var keylist =[];
				var valuelist =[];
				for (var key in response.data[0]) {
					keylist.push(key);
					valuelist.push(response.data[0][key]);
				}
				this.barChartData = {
					keylist: keylist,
					valuelist: valuelist
				}
			})

		},
		initalizeStack1Chart(){
			axios
			.get(`https://827041f5-fbbb-43fd-a419-58cd8b6e55ea.mock.pstmn.io?g=3`)
			.then(response => {
				var keylist =[];
				var valuelist =[];
				console.log(response.data[0])
				for (var key in response.data[0]) {
					
				
					keylist.push(key);
					valuelist.push(response.data[0][key]);
				}
				this.stack1ChartData = {
					keylist: keylist,
					valuelist: valuelist
				}
			})

		},

	}
})
